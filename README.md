# A Simple MitM Script #

![](https://img.shields.io/badge/mitm-python_2.7-blue.svg?style=flat-square) ![](https://img.shields.io/badge/dependencies-driftnet_urlsnarf_dsniff_webspy-orange.svg?style=flat-square)

***

### Usage: ###
```
sudo ./mitm.py
```


**[Screehshot](https://drive.google.com/open?id=0B79r4wTVj-CZZndCdUtFbEk0U28)** :wink:

## After Target Poisoned: ##
**Run sniffers in external terminals**

**Note:** *Your local interface may differ*

**1. driftnet -i eth0**

  *driftnet* -- Will capture all image traffic, when victim browse a website.

**2. urlsnarf -i eth0**

  *urlsnarf* -- Captures all victim's website addresses visited by victim's machine. 
   
**3. dsniff -i eth0** 
 
  *dnsiff* -- Sniffs cleartext passwords. Unique authentication attempts.
  
**4. webspy - Start your favorite browser from the command line: e.g. iceweasel**

  *webspy* -- Watches in real-time as the victim surfs, your browser surfs along with them, automagically.

**5. Surf from the victim's machine**

Note: webspy autostart, just run the browser.